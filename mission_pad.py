#!/usr/bin/python3

import socket
import time

host = ''
port = 9000
locaddr = (host,port) 

# Create a UDP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

sock.bind(locaddr)
tello_address = ('192.168.10.1', 8889)
sock.sendto("command".encode(encoding="utf-8"), tello_address)
time.sleep(1)
sock.sendto("takeoff".encode(encoding="utf-8"), tello_address)
time.sleep(5)
sock.sendto("mon".encode(encoding="utf-8"), tello_address)
time.sleep(5)
sock.sendto("go 100 100 100 60 m1".encode(encoding="utf-8"), tello_address)
time.sleep(9)
sock.sendto("moff".encode(encoding="utf-8"), tello_address)
time.sleep(3)
sock.sendto("land".encode(encoding="utf-8"), tello_address)
time.sleep(9)
sock.close()


